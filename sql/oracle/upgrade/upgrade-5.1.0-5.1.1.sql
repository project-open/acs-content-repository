-- 

-- @author Dave Bauer (dave@thedesignexperience.org)
-- @creation-date 2004-05-02
-- @cvs-id $Id: upgrade-5.1.0-5.1.1.sql,v 1.1.2.3 2004/06/01 23:24:45 donb Exp $
--

-- reload packages to get fixed version of content_folder.rename

@@ ../packages-create.sql
